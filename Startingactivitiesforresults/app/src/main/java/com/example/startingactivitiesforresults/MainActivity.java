package com.example.startingactivitiesforresults;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void CallSecondActivity(View view) {
        EditText name = findViewById(R.id.etName);
        EditText phone = findViewById(R.id.etAge);
        String stringName = name.getText().toString();
        String stringPhone = phone.getText().toString();
        Intent i = new Intent(this, SecondActivity.class);
        i.putExtra("yourname", stringName);
        i.putExtra("yourphone",stringPhone);
        startActivityForResult(i,RESULT_CANCELED);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK && requestCode == RESULT_CANCELED) {
            if (data.hasExtra("returnkey")) {
                String result = data.getExtras().getString("returnkey");
                if (result != null && result.length() > 0) {
                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (resultCode == RESULT_CANCELED && requestCode == RESULT_CANCELED) {
            Toast.makeText(this, data.getExtras().getString("returnkey"),
                    Toast.LENGTH_SHORT).show();
        }
    }
}